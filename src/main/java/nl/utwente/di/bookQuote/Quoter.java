package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private final Map<String, Double> map = new HashMap<>();

    public Quoter() {
        map.put("1", 10.0);
        map.put("2", 45.0);
        map.put("3", 20.0);
        map.put("4", 35.0);
        map.put("5", 50.0);
    }

    public double getBookPrice(String name) {
        return map.getOrDefault(name, 0.0);
    }
}
